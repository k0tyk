/** @license 0BSD 2020 k0tyk <k0tyk@pm.me> */
import { createElement as h } from "./.crank/index.js";
import { Todo } from "./m.js";
class Props {
  location = location;
  method = "";
  title = "";
  xs = [""];
}
/** @param {Props} props */ export async function c(props) {
  var { TodoQ } = await import("./.mq.js");
  var { Repo } = await import("./repo.js");
  var m = /^\?done\.set=([01])\&id\.eq=(.+)$/.exec(props.location.search);
  if (m)
    new Repo(Todo).patch(
      { done: Number(m[1]) },
      new TodoQ().id.eq(decodeURIComponent(m[2]))
    );
  return { ...props, title: "TodoV", xs: new Repo(Todo).get(new TodoQ()) };
}
/** @param {Props} props */ export function* v(props) {
  var m = props;
  this.addEventListener("click", (ev) => {
    if (/^done/.test(ev.target.id))
      for (var x of m.xs)
        if (x.id == ev.target.value)
          fetch(
            `/todo?done.set=${x.done ? 0 : 1}&id.eq=${encodeURIComponent(x.id)}`
          )
            .then(async (x) => ((m = await x.json()), this.refresh()))
            .catch((e) => alert(`${e.message}\n${e.stack}`));
  });
  while (true)
    yield h("form", { style: { "line-height": 1.5, padding: "0 4em" } }, [
      h("h1", null, m.title),
      m.xs.map((x) =>
        h("p", { id: x.id }, [
          h("input", {
            checked: x.done ? "checked" : null,
            id: `done${x.id}`,
            name: "id.eq",
            type: "checkbox",
            value: x.id,
          }),
          h("label", { for: `done${x.id}` }, x.text),
        ])
      ),
    ]);
}
