# k0tyk - quickjs server-side rendered crank mvc website

Install from [pkgsrc](//pkgsrc.org):

```sh
pkgin install curl quickjs ucspi-tcp
curl https://registry.npmjs.org/@bikeshaving/crank/-/crank-0.1.3.tgz |
  tar xz; mv package/esm .crank; rm -r package
tcpserver 127.0.0.1 8080 ./k0tyk.js
```

Test:

```sh
./repo.js # Generate model queries.
for i in *.test; do ./$i.js |diff -u $i - &&echo ok $i ||echo fail $i; done
```

Support old browser:

```sh
sed -i '/event-target-shim/d;s/ extends EventTargetShim//' .crank/events.d.ts
pkgin install yarn; yarn global add typescript
for i in *v.js; do ./bundle.js $i.js; done
```

[0BSD public domain](LICENSE) equivalent license.
