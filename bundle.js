#!/usr/bin/qjs -m
/* Build a page with its dependencies for ES5 browsers.
 * ./bundle.js todov.js; cat .todov.js
 * @license 0BSD 2020 k0tyk <k0tyk@pm.me> */
import * as os from "os";
import * as std from "std";
os.exec([
  "tsc",
  "--allowJs",
  "--lib",
  "DOM,ESNext",
  "-m",
  "system",
  "--outFile",
  `.${scriptArgs[1]}`,
  "--target",
  "ES5",
  ".crank/index.js",
  scriptArgs[1],
]);
var out = std.loadFile(`.${scriptArgs[1]}`);
std.open(`.${scriptArgs[1]}`, "w").puts(`function System(x) {
  if (!/^\\.\\//.test(x)) x = "./" + x;
  if (!/\\.js$/.test(x)) x = x + ".js";
  return x;
}
System.import = function (x) {
  var s = System[(x = System(x))];
  if (s.c) return s.v;
  s.d.map(function (y, i) {
    return s.m.setters[i](System.import(y));
  });
  return s.m.execute(), s.c++, s.v;
};
System.register = function (x, d, f) {
  var s = { c: 0, d: d.map(System), m: {}, v: {} };
  s.m = f(function (k, v) {
    s.v[k] = v;
  });
  System[System(x)] = s;
};
${out.replace(/^#!.*/, "")}`);
