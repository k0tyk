#!/usr/bin/qjs -m
/** @license 0BSD 2020 k0tyk <k0tyk@pm.me> */
import * as std from "std";
import { createElement as h } from "./.crank/index.js";
import { renderer } from "./.crank/html.js";
var m = `${std.in.getline()}`.split(" ") || ["", ""];
var [method, URL] = m;
/** @type {{ [x: string]: string }} */ var headers = {};
for (var x = ""; (x = std.in.getline()) && (x = x.trim()); )
  if ((m = /(.+?): (.*)/.exec(x) || ["", "", ""])[0]) headers[m[1]] = m[2];
try {
  var f = std.open(std.getenv("PWD") + URL, "r");
} catch (_) {}
if (f) for (var x = "", y = ""; (y = f.getline()) != null; x += y + "\n");
if (x) {
  console.log("HTTP/1.1 200 OK");
  console.log(`Content-Type: application/javascript; charset=utf-8\n\n${x}`);
  std.exit();
}
var [pathname, search] = URL.split("?");
if (pathname == "/") pathname = "/index";
if (search) search = `?${search}`;
(async () => {
  var { c, v } = await import(std.getenv("PWD") + pathname + "v.js");
  var props = await c({ headers, location: { pathname, search }, method });
  if (!/html/.test(headers["Accept"] || "")) {
    x = JSON.stringify(props);
    console.log("HTTP/1.1 200 OK");
    console.log(`Content-Type: application/json; charset=utf-8\n\n${x}`);
    return;
  }
  x = `<title>${props.title}</title>${renderer.render(h(v, props))}`;
  var bundle = std.loadFile(
    `${std.getenv("PWD")}${pathname.replace("/", "/.")}v.js`
  );
  x += bundle
    ? `<script>${bundle}</script>`
    : `<script type=module>import { createElement } from "/.crank/index.js";
import { renderer } from "/.crank/dom.js";
import { v } from "${pathname}v.js";
renderer.render(createElement(v, ${JSON.stringify(props)}), document.body);
</script>`;
  console.log("HTTP/1.1 200 OK");
  console.log(`Content-Type: text/html; charset=utf-8\n\n${x}`);
})().catch((e) => {
  x = `${e.message}\n${e.stack}`;
  console.log("HTTP/1.1 500 Internal Server Error");
  console.log(`Content-Type: text/plain; charset=utf-8\n\n${x}`);
});
