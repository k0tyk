#!/usr/bin/qjs -m
/** @license 0BSD 2020 k0tyk <k0tyk@pm.me> */
import * as os from "os";
import * as std from "std";
import * as M from "./m.js";
var mq = std.open(std.getenv("PWD") + "/.mq.js", "w+");
for (var i in M) {
  var m = new M[i]();
  mq.puts(`export class ${Object.getPrototypeOf(m).constructor.name}Q {
  q = {
    filters: [{ k: "", t: "", /** @type {any[]} */ v: [] }].splice(),
    limit: 0,
    offset: 0,
    order: [{ k: "", t: "" }].splice(),
  };
  constructor() {\n`);
  for (var k in m) {
    mq.puts(`    this.${k} = {\n`);
    for (var t of ["eq"])
      mq.puts(`      /** @param {${typeof m[k]}} v */ eq: (v) => (
        this.q.filters.push({ k: "${k}", t: "eq", v: [v] }), this
      ),\n`);
    mq.puts(`      /** @param {${typeof m[k]}[]} v */ in: (v) => (
        this.q.filters.push({ k: "${k}", t: "in", v }), this
      ),
    };\n`);
  }
  mq.puts(`    this.order = {\n`);
  for (var k in m)
    mq.puts(`      ${k}: {
        asc: () => (this.q.order.push({ k: "${k}", t: "asc" }), this),
        desc: () => (this.q.order.push({ k: "${k}", t: "desc" }), this),
      },\n`);
  mq.puts(`    };
    /** @param {number} v */ this.limit = (v) => ((this.q.limit = v), this);
    /** @param {number} v */ this.offset = (v) => ((this.q.offset = v), this);
  }
}\n`);
}
class Q {
  filters = [{ k: "", t: "", /** @type {any[]} */ v: [] }].splice();
  limit = 0;
  offset = 0;
  order = [{ k: "", t: "" }].splice();
}
export class Repo {
  q = new Q();
  /** @param {new () => any} m */ constructor(m) {
    os.mkdir((this.d = std.getenv("PWD") + "/." + (this.m = m).name));
  }
  /** @param {any} a @param {any} b */ order = (a, b) => {
    for (var { k, t } of this.q.order) {
      if (a[k] > b[k]) return t == "asc" ? 1 : -1;
      if (a[k] < b[k]) return t == "asc" ? -1 : 1;
    }
    return 0;
  };
  /** @param {any} x */ where = (x) => {
    for (var { k, t, v } of this.q.filters) {
      if (t == "eq" && x[k] == v[0]) continue;
      if (t == "gte" && x[k] >= v[0]) continue;
      if (t == "in" && v.indexOf(x[k]) !== -1) continue;
      if (t == "lte" && x[k] <= v[0]) continue;
      if (t == "not.eq" && x[k] !== v[0]) continue;
      if (t == "not.gte" && x[k] < v[0]) continue;
      if (t == "not.in" && v.indexOf(x[k]) == -1) continue;
      if (t == "not.lte" && x[k] > v[0]) continue;
      return false;
    }
    return true;
  };
  /** @param {{ q: Q }} mq */ delete(mq) {
    os.exec([
      "rm",
      "-r",
      "--",
      ...this.get(mq).map((x) => `${this.d}/${x.id}`),
    ]);
  }
  /** @param {{ q: Q }} mq */ get(mq) {
    var q = (this.q = mq.q);
    return os
      .readdir(this.d)[0]
      .filter((x) => !/^\.\.?$/.test(x))
      .map((id) => {
        var x = new this.m();
        for (var k in x) {
          var v = std.loadFile(`${this.d}/${id}/${k}`);
          x[k] = typeof x[k] == "number" ? Number(v) : v;
        }
        return x;
      })
      .sort(this.order)
      .filter(this.where)
      .slice(q.offset || 0, (q.offset || 0) + (q.limit || Number.MAX_VALUE));
  }
  /** @param {any} diff @param {{ q: Q }} mq */ patch(diff, mq) {
    for (var x of this.get(mq))
      for (var k in new this.m())
        if (k in diff && x != "id")
          std.open(`${this.d}/${x.id}/${k}`, "w").puts(diff[k]);
  }
  /** @param {any[]} xs */ post(xs) {
    for (var x of xs) {
      os.mkdir(this.d + "/" + x.id);
      for (var k in new this.m())
        std.open(`${this.d}/${x.id}/${k}`, "w").puts(x[k]);
    }
  }
}
