/** @license 0BSD 2020 k0tyk <k0tyk@pm.me> */
import { createElement as h } from "./.crank/index.js";
class Props {
  /** @type {{ [x: string]: string }} */ headers = {};
  location = location;
  method = "";
  xs = [""];
}
/** @param {Props} props */ export async function c(props) {
  var os = await import("os");
  var std = await import("std");
  return { ...props, title: "Index", xs: os.readdir(std.getenv("PWD")) };
}
/** @param {Props} props */ export function v(props) {
  var { headers, location, method, xs } = props;
  var x = JSON.stringify({ headers, location, method }, null, 2);
  return h("div", { style: { "line-height": 1.5, padding: "0 4em" } }, [
    h("h1", null, "List files in project directory."),
    h("pre", null, x + "\n" + JSON.stringify(xs)),
  ]);
}
