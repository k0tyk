#!/usr/bin/qjs
/** @license 0BSD 2020 k0tyk <k0tyk@pm.me> */
import { TodoQ } from "./.mq.js";
import { Todo } from "./m.js";
import { Repo } from "./repo.js";
var repo = new Repo(Todo);
repo.post([{ done: 0, id: "1586905544450", text: "Write quickjs todomvc" }]);
console.log(JSON.stringify(repo.get(new TodoQ())));
console.log(JSON.stringify(repo.get(new TodoQ().id.eq(`${Date.now()}`))));
console.log(JSON.stringify(repo.get(new TodoQ().id.in(["1586905544450"]))));
console.log(JSON.stringify(repo.get(new TodoQ().done.eq(0))));
repo.delete(new TodoQ().done.eq(0));
console.log(JSON.stringify(repo.get(new TodoQ())));
repo.post([{ done: 0, id: "more interesting id", text: "" }]);
console.log(JSON.stringify(repo.get(new TodoQ())));
repo.patch({ done: 1 }, new TodoQ());
console.log(JSON.stringify(repo.get(new TodoQ())));
repo.delete(new TodoQ());
console.log(JSON.stringify(repo.get(new TodoQ())));
